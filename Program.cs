﻿using System;

namespace Carros
{
    class Carro
    {
        
        static void Main(string[] args)
        {
            /* string puertas;
            string combustible;
            string desempeño;
            string velocidadMax; */

           string modelo, color, cilindraje; //atributos
           PedirDatos(out modelo, out color, out cilindraje);
           decimal valor; 
           valor = CalcularValor(modelo, cilindraje);
           MostrarResultados(valor,color);

        }
        //public void setModelo(string n) 
        //{
        //    modelo = n;
        //}
        //public string getModelo()
        //{
          //  return modelo;
        //}
        static void PedirDatos(out string modelo, out string color, out string cilindraje) //atributos a pedir
        {
            do
            {
                Console.Write("Modelo del carro: [B]asico, [M]edio, [L]ujo....? ");
                modelo = Console.ReadLine();
                modelo = modelo.ToUpper(); // Convertir la letra esperada en mayusculas
                if(modelo != "B" && modelo != "M" && modelo != "L") //Envia mensaje al usuario de que dato espera
                {
                    Console.WriteLine("Opcion no valida");
                }
            } while(modelo != "B" && modelo != "M" && modelo != "L"); // Valida el dato esperado
            do
            {
                Console.Write("Color del carro: [A]zul, [N]egro, [R]ojo, [P]lata....? ");
                color = Console.ReadLine();
                color = color.ToUpper(); // Convertir la letra esperada en mayusculas
                if(color != "A" && color != "N" && color != "R" && color != "P") //Envia mensaje al usuario de que dato espera
                {
                    Console.WriteLine("Opcion no valida");
                }
            } while(color != "A" && color != "N" && color != "R" && color != "P"); // Valida el dato esperado
            do
            {
                Console.Write("Cilindraje deseado del carro: [A]4cil, [B]6cil, [C]8cil....? ");
                cilindraje = Console.ReadLine();
                cilindraje = cilindraje.ToUpper(); // Convertir la letra esperada en mayusculas
                if(cilindraje != "A" && cilindraje != "B" && cilindraje != "C") //Envia mensaje al usuario de que dato espera
                {
                    Console.WriteLine("Opcion no valida");
                }
            } while(cilindraje != "A" && cilindraje != "B" && cilindraje != "C"); // Valida el dato esperado
            /*do
            {
                Console.Write("Cantidad de puertas: [A]3puertas, [B]4puertas....? ");
                puertas = Console.ReadLine();
                puertas = puertas.ToUpper(); // Convertir la letra esperada en mayusculas
                if(puertas != "A" && puertas != "B") //Envia mensaje al usuario de que dato espera
                {
                    Console.WriteLine("Opcion no valida");
                }
            } while(puertas != "A" && puertas != "B"); // Valida el dato esperado */
            /*do
            {
                Console.Write("Tipo de combustible: [A]Magna, [B]Premium, [C]Dicel....? ");
                combustible = Console.ReadLine();
                combustible = combustible.ToUpper(); // Convertir la letra esperada en mayusculas
                if(combustible != "A" && combustible != "B" && combustible != "C") //Envia mensaje al usuario de que dato espera
                {
                    Console.WriteLine("Opcion no valida");
                }
            } while(combustible != "A" && combustible != "B" && combustible != "C"); // Valida el dato esperado */

        }
        static decimal CalcularValor(string modelo, string cilindraje) //Metodo
        {
            decimal valor;
            if(modelo == "B") // Modelo
            {
                if(cilindraje == "A") valor = 1;
                else if(cilindraje == "B") valor = 2;
                else valor = 3;
            }
            else if(modelo == "M")
            {
                if(cilindraje == "A") valor = 4;
                else if(cilindraje == "B") valor = 5;
                else valor = 6;
            }
            else 
            {
                if(cilindraje == "A") valor = 7;
                else if(cilindraje == "B") valor = 8;
                else valor = 9;
            }
            return valor;
        }
        static void MostrarResultados(decimal valor, string color) //Metodo
        {
            string modelo="", puertas="", combustible="", desempeño="", velocidadMax="", textColor="";

            if(valor < 4)
            { 
               modelo = "Basico";
               puertas = "4 puertas";
               combustible = "Magna";
            
                if(valor == 1)
                    {
                        desempeño = "4km / L";
                        velocidadMax = "180km / h";
                    }
                    else if(valor == 2)
                    {
                        desempeño = "3km / L";
                        velocidadMax = "200km / h";
                    }
                    else
                    {
                        desempeño = "2km / L";
                        velocidadMax = "220km / h";
                    }
            }
            else if(valor > 3 & valor < 7)
            { 
               modelo = "Medio";
               puertas = "4 puertas";
               combustible = "Premium";
            
                if(valor == 4)
                    {
                        desempeño = "6km / L";
                        velocidadMax = "200km / h";
                    }
                    else if(valor == 5)
                    {
                        desempeño = "5km / L";
                        velocidadMax = "220km / h";
                    }
                    else
                    {
                        desempeño = "4km / L";
                        velocidadMax = "240km / h";
                    }
            }
            else if(valor > 6 )
            {
                modelo = "Lujo";
                puertas = "2 puertas";
                combustible = "Premium";
            
                if(valor == 7)
                    {
                        desempeño = "6km / L";
                        velocidadMax = "240km / h";
                    }
                    else if(valor == 8)
                    {
                        desempeño = "5km / L";
                        velocidadMax = "260km / h";
                    }
                    else
                    {
                        desempeño = "4km / L";
                        velocidadMax = "280km / h";
                    }
            }
            if(color == "A")
            {
                textColor = "Azul";
            }
            else if(color == "N")
            {
                textColor = "Negro";
            }
            else if(color == "R")
            {
                textColor = "Rojo";
            }
            else textColor = "Plata";

            Console.WriteLine("El modelo elegido es: {0}",modelo);
            Console.WriteLine("El Color elegido es: {0}",textColor);
            Console.WriteLine("El Numero de puertas elegido es: {0}",puertas);
            Console.WriteLine("El combustible del carro es: {0}",combustible);
            Console.WriteLine("El desempeño del carro es: {0}",desempeño);
            Console.WriteLine("La velocidad maxima es: {0}",velocidadMax);
            Console.ReadKey();
        }

    }
}
